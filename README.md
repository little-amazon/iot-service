# product-service
This module contains the IOT service module

![Flamingo image](https://cdn1.vectorstock.com/i/1000x1000/27/05/flamingo-cartoon-vector-892705.jpg)

# Description
A docker file is created to run nodejs
The image has all the file inside and expose PORT 8080
.dockerignore is included to ignore all the dependencies
Dependencies will be installed by the RUN command

# How to build image
Simply run
    docker build -t littleamazon/iot-service-img .

# How to run image
Simply run
    > docker run -d --name iotcontainer -p 8080:8080 --link redis_db_container:db_service  littleamazon/iot-service-img
     

:fireworks: enjoy :fireworks:
