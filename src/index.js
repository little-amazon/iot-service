

const express = require('express');
const router = require('./router/router.js'); //get the router OBJ
const bodyParser = require('body-parser'); //to correct parse json from body



// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

//add json parser POST function
app.use(bodyParser.json());

//add router
app.use(router);

//start APP
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
