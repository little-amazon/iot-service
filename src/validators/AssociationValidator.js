const { body } = require('express-validator/check');

module.exports.createAssociationValidator = [
  body('userID', 'userID is required').exists(),
  body('userID', 'userID must be a string').isString(),
  body('productID', 'productID  is required').exists(),
  body('productID', 'productID  must be a string').isString(),
  body('quantity', 'quantity is required').exists(),
  body('quantity', 'quantity must be a integer').isInt()

];
