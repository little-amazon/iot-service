'use strict';

const { validationResult } = require('express-validator/check');

function AssociationValidatorResult (req, res, next) {
  let errors = validationResult(req);

  if (errors.isEmpty()) {
    return next();
  }

  let firstError = errors.array()[0];

  return res.status(400).json({ code: 'ValidationError', message: firstError.msg });
}

module.exports = {
  AssociationValidatorResult: AssociationValidatorResult
};
