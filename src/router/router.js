const Router = require('express').Router;
//const helloWorldController = require('../controllers/helloWorld.js').helloWorldCallback
const get__all_associations_controller = require('../controllers/io_associations_controller.js').associationsCallback
const remove__all_associations_controller = require('../controllers/io_associations_controller.js').removeAssociationsController
const add_new_association_controller = require('../controllers/io_associations_controller.js').add_new_association_controller
const get_association_for_userID_controller = require('../controllers/io_associations_controller.js').get_association_for_userID_controller
const get_association_for_associationID = require('../controllers/io_associations_controller.js').get_association_for_associationID

//VALIDATORS
const createAssociationValidator = require('../validators/AssociationValidator.js').createAssociationValidator

//CHECKERS (Middleware)
const associationValidatorResult = require('../checkers/AssociationChecker.js').AssociationValidatorResult

//init router
const router = Router();

//test call
//router.get('/hello', helloWorldController);

//retrive all the associations
router.get('/iot/association', get__all_associations_controller)

//delete all the associations
router.delete('/iot/association',remove__all_associations_controller )

// //according to express-validator lib -> path, validator, check to return errors, req/res function if all is correct
//create new association
router.post('/iot/association',createAssociationValidator,associationValidatorResult,add_new_association_controller)

//retrive associations for an userID
router.get('/iot/association/user/:userID',get_association_for_userID_controller )

//retrive association given association ID
router.get('/iot/association/:associationID',get_association_for_associationID )



module.exports = router;
