
var db = require('../database/db.js')


//==============================================================================
async function associationsCallback(req, resP){

    //1. use database
    db.get_all_items().then(function resolve(res){

      res.length != 0 ?
      resP.status(200).send(res) :
      resP.status(404).json({ reason: "No associations found"})

    }, function err(err){
      resP.status(400).json({ reason: err})
    })


}

//==============================================================================
async function remove__all_associations_controller(req,res){

  // //1. user db
  db.delete_all_items_stored().then(function (resolve) {

      res.status(200).json({ reason: "items deleted"})


  }, function (reject) {

      res.status(400).json({ reason: reject})
  })
}
//==============================================================================
async function add_new_association_controller(req, res){


    //1. create json


    db.add_new_association(req.body.userID, req.body.productID, req.body.quantity)

    res.status(200).json({reason : "Inserted"})
}
//==============================================================================
async function get_association_for_userID_controller(req, res){

  db.get_associations_for_userID(req.params.userID).then(function (succ){

    //determinates if user exists
    if(succ == null){
      res.status(404).json({reason: "associations with that user ID not found"})
    }else{
      res.status(200).json(succ)
    }

  }, function(err){
    res.status(400).json({error: err})
  })


}
//==============================================================================
async function get_association_for_associationID(req, res){

  db.require_association(req.params.associationID).then(function( success) {
    res.status(200).json(success)
  }, function(error){
    res.status(400).json({error: error})
  })

}
//==============================================================================
async function delete_association_for_associationID(req,res){

  db.delete_association(req.params.associationID).then(function( success) {
    res.status(200).json(success)
  }, function(error){
    res.status(400).json({error: error})
  })

}
module.exports = {
  associationsCallback : associationsCallback,
  removeAssociationsController: remove__all_associations_controller,
  add_new_association_controller: add_new_association_controller,
  get_association_for_userID_controller: get_association_for_userID_controller,
  get_association_for_associationID: get_association_for_associationID,
  delete_association_for_associationID: delete_association_for_associationID
}
