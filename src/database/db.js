const redis = require("redis")

//connection attributes
const HOST =  process.env.IOT_DB_URL //'db_service'//'192.168.99.100'//
const PORT = process.env.IOT_DB_PORT //6379


client = redis.createClient(PORT, HOST);


client.on('connect', function() {
    console.log('Redis client connected');
});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
    process.exit(1);
});


//==========================
//get all items stored
//==========================

async function get_all_items(){

    console.log("Get call items invocated")

   return  new Promise(function(resolve, reject) {

      //1. get all the keys
      client.keys('*', (err, keys) => {

        //2.A check if error
        if(err){
          console.log("Error")
          reject(err)
        }else{

          //2.B Create empty array with the keys
          var values_array = []

          //2.B check the keys
          if(keys.length != 0){

            console.log("KEYS: "+ keys)

            //3. Extract values and return an array with that
            keys.forEach((current_key, index)=> {

              client.get(current_key, function (err, value) {
                  if (err) throw(err);
                  //console.log(value)
                  values_array.push(JSON.parse(value))

                  //if is the last call callback
                  if(index + 1 == keys.length){
                      resolve(values_array)
                  }
                })



            })//end forEach

          }else{
            console.log("No keys")
              resolve(values_array)
          }

          //4. return value for each keys
          console.log(values_array)
          //resolve(values_array)

        }//end else - no error
      })//end get all keys
   })//end promise
 }//return get_all_items


 //==========================
 //delete all items stored
 //==========================
 async function delete_all_items_stored(){

   return new Promise(function (resolve, reject){

     //1. get all the keys
     client.keys('*', (err, keys) => {

       //2.A check if error
       if(err){
         console.log("Error")
         reject(err)
       }else{

         //2.B Create empty array with the keys
         console.log("Keys found: "+ keys)

         //2.C controll if there are KEYS
         if(keys.length == 0){
           resolve()
           return
         }


         //3. remove each keys
         keys.forEach((value, index)=>{

           client.del(value, (err,response) =>{

             if(err){
               reject(err)
             }else{

               if(index + 1 == keys.length){
                 resolve()
               }
             }

           })//end del

         })//end forEach


       }//else error
     })//end get all keys
   })//end Promise


 }//end  async fucntion delete_all_items_stored


 //==========================
 // ADD ITEMS
 //==========================
 async function add_new_association(userID, productID, quantity){

   return new Promise( function (resolve, reject) {

     //1. get the  previous associations if any
     client.get(userID, function (err, value){

        if(value == null){

          //2. insert new value
          client.set(userID, JSON.stringify({userID: userID, associations: [{associationID: new Date().getTime(),userID: userID, productID : productID, quantity : quantity}]}))


        }else{

          //3. add to the current array the new associationJSON
          //console.log("Value: "+value)
          var newValue = JSON.parse(value)
          //console.log("ValueJSON: "+newValue)

          newValue.associations.push({associationID: new Date().getTime(), userID: userID, productID : productID, quantity : quantity})

          client.set(userID, JSON.stringify(newValue))


        }

     })

  })

 }

 //==========================
 // REQUIRE ASSOCIATIONS FOR USER
 //==========================
async function get_associations_for_userID(userID){

  return new Promise(function (res,rej){

    client.get(userID, function( err, succ){

      if(err){
        rej(err)
      }else{
        res(JSON.parse(succ))
      }
    })
  })

}
//==========================================================
// REQUIRE ASSOCIATIONS FOR ASSOCIATION ID
//==========================================================
async function require_association(associationID){
  return new Promise(function (resolveA, reject){

      //1. get all info
      get_all_items().then(function (resolve){

        //2. identify if there is a association with that // IDEA:
        var association_founded = null

        resolve.forEach(function(value, index){

          //single JSON
          var singleJSON = value//JSON.parse(value)

          console.log("Single value: "+singleJSON)
          console.log("Single ass: "+singleJSON.associations)
          singleJSON.associations.forEach(function (ass, indA){

              if(ass.associationID == associationID){
                association_founded = ass

              }
          })

        })

        //3. resolve
        resolveA(association_founded)


      }, function(error){

          reject(error)
      })
  })
}


 module.exports = {
   get_all_items :  get_all_items,
   delete_all_items_stored: delete_all_items_stored,
   add_new_association : add_new_association,
   get_associations_for_userID: get_associations_for_userID,
   require_association: require_association
 }
