# How to use this img

Create a couch Redis img from Dockerfile
> docker build -t redis_db_img .

Create a cointainer and expose the port 6379
> docker run -d --name redis_db_container -p 6379:6379  redis_db_img

## VOLUMES

This Dockerfile create one volume to perseve storage and configurations.

> VOLUME /var/log /data
